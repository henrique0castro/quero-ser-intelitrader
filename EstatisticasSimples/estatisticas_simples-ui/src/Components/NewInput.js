import React, { Component } from 'react';

export default class NewInput extends Component {
    render() {
        return (
            <input className="number_input" type="number" id={this.props.valor} name={this.props.nome} />
        )
    }
}