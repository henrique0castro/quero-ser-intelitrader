import React from 'react';
import { thisExpression } from '@babel/types';
import ReactDOM from 'react-dom';
import Input from "./NewInput";
class MainArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputs: ['input-0', 'input-1'],
            values: []
        };
    };

    appendInput() {
        if (this.state.inputs.length < 30) {
            var newInput = `input-${this.state.inputs.length}`;
            // prevState pega o estado da ultima modificacao
            this.setState(prevState => ({ inputs: prevState.inputs.concat([newInput]) }));
        }
        else {
            window.alert('Esta página permite apenas 29 caixas.')
        }
    }

    // getCircularReplacer(){
    //     const seen = new WeakSet();
    //     return (key, value) => {
    //       if (typeof value === "object" && value !== null) {
    //         if (seen.has(value)) {
    //           return;
    //         }
    //         seen.add(value);
    //       }
    //       return value;
    //     };
    //   };

    rankNumber = () => {
        console.log(this.state.inputs);
        console.log(document.getElementById('dynamicInput').children)
        const dynamicInput = document.getElementById('dynamicInput');
        
        for (let index = 0; index < dynamicInput.children.length; index++) {
            this.state.values.push(dynamicInput.children.item(index));
        }

        let sequencia = [];
        while (sequencia.length > 0) {
            sequencia.pop();
        }
        this.state.values.forEach(value => {
            sequencia.push(value.value);
        });

        let valorMinimo = Math.min.apply(Math, sequencia);

        let valorMaximo = Math.max.apply(Math, sequencia);

        let quantidade = sequencia.length;

        let soma = 0;
        for (let index = 0; index < sequencia.length; index++) {
            soma = soma + parseInt(sequencia[index], 10);
        }

        let valorMedio = soma / sequencia.length;
        var response = [
            {
                sequencia: sequencia,
                valorMinimo: valorMinimo,
                valorMaximo: valorMaximo,
                quantidade: quantidade,
                valorMedio: valorMedio
            }
        ];
      
        this.setState({ values : response});
        this.state.values.map(x => {
            console.log(x.sequencia);
        })

        if (!dynamicInput.hasChildNodes) {
            window.alert('vish');
        }
        else {
            for (let index = 0; index < dynamicInput.children.length; index++) {
                this.state.values.push(dynamicInput.children.item(index));
            }

            let sequencia = [];
            while (sequencia.length > 0) {
                sequencia.pop();
            }
            this.state.values.forEach(value => {
                sequencia.push(value.value);
            });

            let valorMinimo = Math.min.apply(Math, sequencia);

            let valorMaximo = Math.max.apply(Math, sequencia);

            let quantidade = sequencia.length;

            let soma = 0;
            for (let index = 0; index < sequencia.length; index++) {
                soma = soma + parseInt(sequencia[index], 10);
            }

            let valorMedio = soma / sequencia.length;
            var response = [
                {
                    sequencia: sequencia,
                    valorMinimo: valorMinimo,
                    valorMaximo: valorMaximo,
                    quantidade: quantidade,
                    valorMedio: valorMedio
                }
            ];
            this.state.values = response;
            this.state.values.map(x => {
                console.log(x.sequencia);
            })

        }

    }

    render() {
        return (
            <div>
                <div className="flex-box">
                    <button onClick={() => this.appendInput()}>+</button>
                    <form>
                        <div id="dynamicInput">
                            {this.state.inputs.map(input => <Input key={input} nome={input} />)}
                        </div>
                    </form>
                    <button onClick={() => this.rankNumber()}> Classificar </button>
                    <button onClick={() => document.location.reload()}>Limpar</button>
                </div>
                <div className="table-box">
                    <table>
                        <thead>
                            <tr>
                                <th>Sequência</th>
                                <th>Valor mínimo</th>
                                <th>Valor máximo</th>
                                <th>Quantidade</th>
                                <th>Valor medio</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            {this.state.values.map(x => {
                                return (<tr key={x.sequencia}>
                                    <td>{x.sequencia}</td>
                                    <td>{x.valorMinimo}</td>
                                    <td>{x.valorMaximo}</td>
                                    <td>{x.quantidade}</td>
                                    <td>{x.valorMedio}</td>
                                </tr>)
                            })}
                        </tbody>
                    </table>

                </div>
            </div>
        );
    }

}

export default MainArea;