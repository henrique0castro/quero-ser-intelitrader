import React, { Component } from 'react';
import '../../css/App.css';
import Input from "../../Components/NewInput";
import Main from "../../Components/MainArea";


function App() {
  return (
    <div>
      <header>
        <h1>Insira uma sequência de números</h1>
        <h2>(de acordo com as caixas disponíveis)</h2>
      </header>
      <main>
        <Main />
        
      </main>
    </div>
  );
}
export default App;